'use strict';

let onDev = function(tabUrl) {
    let url = new URL(tabUrl);

    url.host = "dev.gitlab.org";

    let path = url.pathname;

    let mappingRules = [
        { from: "/gitlab-org/gitlab-foss", to: "/gitlab/gitlabhq" },
        { from: "/gitlab-org/gitlab", to: "/gitlab/gitlab-ee" },
        { from: "/gitlab-org/charts/gitlab", to: "/gitlab/charts/gitlab" },
        { from: "/gitlab-org/build/CNG", to: "/gitlab/charts/components/images" },
        { from: "/gitlab-org/", to: "/gitlab/" },
        { from: "/gitlab-com/www-gitlab-com", to: "/gitlab/www-gitlab-com" }
    ];

    for(let rule of mappingRules) {
        if (path.startsWith(rule.from)) {
            url.pathname = rule.to + path.slice(rule.from.length);

            break;
        }
    }

    return url.toString();
};

let showOnDev = document.getElementById('showOnDev');

showOnDev.onclick = function(element) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.create({openerTabId: tabs[0].id, url: onDev(tabs[0].url), active: true});
    });
};
